package com.ifyou.rostmuseum.data

import android.content.Context
import android.widget.Filter
import com.ifyou.rostmuseum.R
import java.util.*

class DataHelper(context: Context) {

    val sMapSuggestions = mapOf(
            Pair("red_palace", MapSuggestion(context.getString(R.string.red_palace))),
            Pair("dom_na_pogrebah", MapSuggestion(context.getString(R.string.dom_na_pogrebah))))

    fun findSuggestions(query: String, limit: Int, simulatedDelay: Long,
                        listener: OnFindSuggestionsListener?) {
        object : Filter() {
            override fun performFiltering(constraint: CharSequence?): FilterResults {
                try {
                    Thread.sleep(simulatedDelay)
                } catch (e: InterruptedException) {
                    e.printStackTrace()
                }

                resetSuggestionsHistory()
                val suggestionListMap: MutableMap<String, MapSuggestion> = mutableMapOf()
                if (!(constraint == null || constraint.isEmpty())) {
                    for ((key, value) in sMapSuggestions) {
                        if (value.body.toUpperCase()
                                .contains(constraint.toString().toUpperCase())) {
                            suggestionListMap.put(key, value)
                            if (limit != -1 && suggestionListMap.size == limit) {
                                break
                            }
                        }
                    }
                }

                val results = FilterResults()
                results.values = suggestionListMap
                results.count = suggestionListMap.size

                return results
            }

            @Suppress("UNCHECKED_CAST")
            override fun publishResults(constraint: CharSequence, results: FilterResults) {
                listener?.onResults(results.values as MutableMap<String, MapSuggestion>)
            }
        }.filter(query)
    }

    fun getHistory(count: Int): List<MapSuggestion> {
        val suggestionList = ArrayList<MapSuggestion>()
        var colorSuggestion: MapSuggestion
        for (i in 0..sMapSuggestions.values.size - 1) {
            colorSuggestion = sMapSuggestions.values.elementAt(i)
            colorSuggestion.setIsHistory(true)
            suggestionList.add(colorSuggestion)
            if (suggestionList.size == count) {
                break
            }
        }
        return suggestionList
    }

    fun resetSuggestionsHistory() {
        for (mapSuggestion in sMapSuggestions.values) {
            mapSuggestion.setIsHistory(false)
        }
    }

    interface OnFindSuggestionsListener {
        fun onResults(results: MutableMap<String, MapSuggestion>)
    }
}
