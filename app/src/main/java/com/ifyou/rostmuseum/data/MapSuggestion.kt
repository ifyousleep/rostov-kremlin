package com.ifyou.rostmuseum.data

import com.arlib.floatingsearchview.suggestions.model.SearchSuggestion
import android.os.Parcel
import android.os.Parcelable


class MapSuggestion : SearchSuggestion {

    private var mPlaceName: String = ""
    var isHistory = false

    constructor(suggestion: String) {
        this.mPlaceName = suggestion.toLowerCase()
    }

    constructor(source: Parcel) {
        this.mPlaceName = source.readString()
        this.isHistory = source.readInt() != 0
    }

    fun setIsHistory(isHistory: Boolean) {
        this.isHistory = isHistory
    }

    override fun getBody(): String {
        return mPlaceName
    }

    override fun describeContents(): Int {
        return 0
    }

    override fun writeToParcel(dest: Parcel, flags: Int) {
        dest.writeString(mPlaceName)
        dest.writeInt(if (isHistory) 1 else 0)
    }

    companion object {

        val CREATOR: Parcelable.Creator<MapSuggestion> = object : Parcelable.Creator<MapSuggestion> {
            override fun createFromParcel(`in`: Parcel): MapSuggestion {
                return MapSuggestion(`in`)
            }

            override fun newArray(size: Int): Array<MapSuggestion?> {
                return arrayOfNulls(size)
            }
        }
    }
}
