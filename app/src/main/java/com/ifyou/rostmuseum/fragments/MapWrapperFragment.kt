package com.ifyou.rostmuseum.fragments

import android.graphics.BitmapFactory
import android.location.Location
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.Marker
import com.google.android.gms.maps.model.MarkerOptions
import com.ifyou.rostmuseum.controllers.LocationManager
import com.google.android.gms.common.ConnectionResult
import com.ifyou.rostmuseum.R
import com.ifyou.rostmuseum.activities.MainActivity
import com.google.android.gms.maps.model.CameraPosition

class MapWrapperFragment : Fragment(), OnMapReadyCallback,
        GoogleMap.OnMarkerClickListener, GoogleMap.OnMapClickListener/*, SensorEventListener*/ {

    private var mView: View? = null
    private var mMap: GoogleMap? = null
    private var mSupportMapFragment: SupportMapFragment? = null
    private var locationManager: LocationManager? = null
    private var mLocation: Location? = null
    //private val mRotationMatrix = FloatArray(16)
    //private var sm: SensorManager? = null

    companion object {
        fun newInstance(): MapWrapperFragment {
            val fragment = MapWrapperFragment()
            return fragment
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        retainInstance = true
    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        /*sm = context.getSystemService(SENSOR_SERVICE) as SensorManager
        if (sm?.getSensorList(Sensor.TYPE_ACCELEROMETER)?.size !== 0) {
            val s = sm!!.getSensorList(Sensor.TYPE_ACCELEROMETER)[0]
            sm?.registerListener(this, s, SensorManager.SENSOR_DELAY_NORMAL)
        }*/

        locationManager = LocationManager.getInstance(context)
        locationManager?.register(object : LocationManager.Listener {
            override fun onLocationChanged(location: Location) {
                if (mLocation == null) {
                    mLocation = location
                    initMap()
                } else {
                    mLocation = location
                }
            }

            override fun onLocationFetchFailed(connectionResult: ConnectionResult?) {

            }
        })
        // Inflate the layout for this fragment if the view is not null
        if (mView == null)
            mView = inflater!!.inflate(R.layout.fragment_map_wrapper, container, false)

        // build the map
        if (mSupportMapFragment == null) {
            mSupportMapFragment = SupportMapFragment.newInstance()
            childFragmentManager.beginTransaction().replace(R.id.map, mSupportMapFragment).commit()
            mSupportMapFragment?.retainInstance = true
        }
        if (mMap == null)
            mSupportMapFragment?.getMapAsync(this)

        return mView
    }

    override fun onMapReady(googleMap: GoogleMap) {
        mMap = googleMap
        val kremlin = LatLng(57.184764, 39.416235)
        val redPalace = LatLng(57.184157, 39.415988)
        val domNaPogrebah = LatLng(57.184972, 39.417120)

        mMap?.addMarker(MarkerOptions().position(redPalace).icon(BitmapDescriptorFactory.fromBitmap(BitmapFactory.decodeResource
        (resources, R.drawable.ic_location))))?.tag = "red_palace"

        mMap?.addMarker(MarkerOptions().position(domNaPogrebah).icon(BitmapDescriptorFactory.fromBitmap(BitmapFactory.decodeResource
        (resources, R.drawable.ic_location))))?.tag = "dom_na_pogrebah"

        mMap?.animateCamera(CameraUpdateFactory.newLatLngZoom(kremlin, 18f))
        mMap?.setOnMarkerClickListener(this)
        mMap?.setOnMapClickListener(this)
    }

    override fun onMarkerClick(marker: Marker): Boolean {
        (activity as MainActivity).showBottomSheetBehavior(marker.tag as String)
        return true
    }

    override fun onMapClick(p0: LatLng?) {
        (activity as MainActivity).clearSearch()
        (activity as MainActivity).clearBottomSheetBehavior()
    }

    private fun initMap() {
        if (view != null) {
            if (mLocation != null && mMap != null) {
                mMap?.isMyLocationEnabled = true
            }
        }
    }

    /*override fun onSensorChanged(event: SensorEvent) {
        if (event.sensor.type == Sensor.TYPE_ROTATION_VECTOR) {
            val field = GeomagneticField(
                    mLocation?.latitude?.toFloat()!!,
                    mLocation?.longitude?.toFloat()!!,
                    mLocation?.altitude?.toFloat()!!,
                    System.currentTimeMillis()
            )
            val mDeclination = field.declination
            SensorManager.getRotationMatrixFromVector(
                    mRotationMatrix, event.values)
            val orientation = FloatArray(3)
            SensorManager.getOrientation(mRotationMatrix, orientation)
            val bearing = Math.toDegrees(orientation[0].toDouble()) + mDeclination
            updateCamera(bearing.toFloat())
        }
    }*/

    /*override fun onAccuracyChanged(sensor: Sensor, accuracy: Int) {

    }

    private fun updateCamera(bearing: Float) {
        val oldPos = mMap?.cameraPosition
        val pos = CameraPosition.builder(oldPos).bearing(bearing).build()
        mMap?.moveCamera(CameraUpdateFactory.newCameraPosition(pos))
    }*/

    /*fun findMe() {
        if (mLocation != null && mMap != null) {
            mMap?.moveCamera(CameraUpdateFactory.newLatLngZoom(LatLng(mLocation!!.latitude, mLocation!!.longitude), 18f))
        } else {

        }
    }*/

    fun updateCameraBearing() {
        if (mMap == null) return
        if (mLocation != null && mMap != null) {
            val currentPlace = CameraPosition.Builder()
                    .target(LatLng(mLocation!!.latitude, mLocation!!.longitude))
                    .bearing(mLocation?.bearing!!)
                    .zoom(18f)
                    .build()
            mMap?.animateCamera(CameraUpdateFactory.newCameraPosition(currentPlace))
        }
    }
}