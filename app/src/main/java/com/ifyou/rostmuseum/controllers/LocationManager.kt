package com.ifyou.rostmuseum.controllers

import android.Manifest
import android.app.Activity
import android.content.Context
import android.content.pm.PackageManager
import android.location.Location
import android.os.Bundle
import android.support.v4.app.ActivityCompat
import android.util.Log

import com.google.android.gms.common.ConnectionResult
import com.google.android.gms.common.api.GoogleApiClient
import com.google.android.gms.location.LocationListener
import com.google.android.gms.location.LocationRequest
import com.google.android.gms.location.LocationServices

import java.util.ArrayList

class LocationManager private constructor(context: Context) {

    companion object {
        private val TAG = "LocationManager"
        private var instance: LocationManager? = null
        fun getInstance(context: Context): LocationManager {
            if (instance == null) {
                instance = LocationManager(context)
            }
            return instance!!
        }
    }

    private val listeners: MutableList<Listener>?
    internal var mGoogleApiClient: GoogleApiClient? = null
    internal var location: Location? = null

    init {
        val locationListener = object : LocationListener {
            override fun onLocationChanged(newLocation: Location) {
                location = newLocation
                Log.d(TAG, "User location found: " + location!!.latitude + ", " + location!!.longitude)
                notifyLocationChanged(location!!)
            }
        }
        if (mGoogleApiClient == null) {
            mGoogleApiClient = GoogleApiClient.Builder(context)
                    .addConnectionCallbacks(object : GoogleApiClient.ConnectionCallbacks {
                        override fun onConnected(bundle: Bundle?) {
                            val mLocationRequest = LocationRequest()
                            mLocationRequest.interval = 10000
                            mLocationRequest.fastestInterval = 5000
                            mLocationRequest.priority = LocationRequest.PRIORITY_HIGH_ACCURACY
                            if (ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                                if (context is Activity) {
                                    ActivityCompat.requestPermissions(context, arrayOf(Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION), 0)
                                }
                                return
                            }
                            LocationServices.FusedLocationApi.requestLocationUpdates(
                                    mGoogleApiClient, mLocationRequest, locationListener)

                        }
                        override fun onConnectionSuspended(i: Int) {
                            Log.e(TAG, "Failed to fetch user location. Connection suspended, code: " + i)
                            notifyLocationFetchFailed(null)
                        }
                    })
                    .addOnConnectionFailedListener { connectionResult ->
                        Log.e(TAG, "Failed to fetch user location. Connection result: " + connectionResult.errorMessage!!)
                        notifyLocationFetchFailed(connectionResult)
                    }
                    .addApi(LocationServices.API)
                    .build()
        }
        listeners = ArrayList<Listener>()
    }

    /*fun getLocation(): LatLng? {
        //Don't getLatitude without checking if location is not null... it will throw sys err...
        if (location != null) {
            return LatLng(location!!.latitude, location!!.longitude)
        } else {
            notifyLocationFetchFailed(null)
        }
        return null
    }*/

    /*fun getLocation(): Float? {
        if (location != null) {
            return location?.bearing
        }
        return null
    }*/

    fun onResume() {
        if (mGoogleApiClient != null) {
            mGoogleApiClient!!.connect()
        }
    }

    fun onPause() {
        if (mGoogleApiClient != null) {
            mGoogleApiClient!!.disconnect()
        }
    }

    fun unregister(listener: Listener) {
        if (listeners != null && listeners.indexOf(listener) != -1) {
            listeners.remove(listener)
        }
    }

    fun register(listener: Listener) {
        if (listeners != null && listeners.indexOf(listener) == -1) {
            listeners.add(listener)
        }
    }

    private fun notifyLocationFetchFailed(connectionResult: ConnectionResult?) {
        if (listeners != null) {
            for (listener in listeners) {
                listener.onLocationFetchFailed(connectionResult)
            }
        }
    }

    private fun notifyLocationChanged(location: Location) {
        if (listeners != null) {
            for (listener in listeners) {
                listener.onLocationChanged(location)
            }
        }
    }

    interface Listener {
        fun onLocationChanged(location: Location)
        fun onLocationFetchFailed(connectionResult: ConnectionResult?)
    }
}