package com.ifyou.rostmuseum.activities

import android.os.Bundle
import android.support.design.widget.BottomSheetBehavior
import android.support.v7.app.AppCompatActivity
import android.view.View
import com.ifyou.rostmuseum.fragments.MapWrapperFragment
import com.ifyou.rostmuseum.R
import com.ifyou.rostmuseum.controllers.LocationManager
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.bottom_sheet.*
import com.arlib.floatingsearchview.FloatingSearchView
import com.arlib.floatingsearchview.suggestions.model.SearchSuggestion
import com.ifyou.rostmuseum.data.DataHelper
import com.ifyou.rostmuseum.data.MapSuggestion

class MainActivity : AppCompatActivity() {

    private val MAP_FRAGMENT_TAG = "MapFragment"
    private lateinit var mBottomSheetBehavior: BottomSheetBehavior<*>
    private var locationListener: LocationManager.Listener? = null
    var locationManager: LocationManager? = null
    private var mLastQuery = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        locationManager = LocationManager.getInstance(this)

        mBottomSheetBehavior = BottomSheetBehavior.from(bottom_sheet_overview)
        mBottomSheetBehavior.isHideable = true
        mBottomSheetBehavior.state = BottomSheetBehavior.STATE_HIDDEN

        mBottomSheetBehavior.setBottomSheetCallback(object : BottomSheetBehavior.BottomSheetCallback() {
            override fun onSlide(bottomSheet: View, slideOffset: Float) {

            }

            override fun onStateChanged(bottomSheet: View, newState: Int) {
                when (newState) {
                    BottomSheetBehavior.STATE_HIDDEN -> fab.animate().scaleX(1f).scaleY(1f).setDuration(300).start()
                    else -> fab.animate().scaleX(0f).scaleY(0f).setDuration(300).start()
                }
            }
        })

        val fragmentManager = supportFragmentManager
        var mapWrapperFragment: MapWrapperFragment? = fragmentManager.findFragmentByTag(MAP_FRAGMENT_TAG) as MapWrapperFragment?
        if (mapWrapperFragment == null) {
            mapWrapperFragment = MapWrapperFragment.newInstance()
        }
        fragmentManager.beginTransaction().replace(R.id.main_container, mapWrapperFragment, MAP_FRAGMENT_TAG)
                .commit()

        fab.setOnClickListener {
            val mapFragment = fragmentManager.findFragmentByTag(MAP_FRAGMENT_TAG) as MapWrapperFragment?
            mapFragment?.updateCameraBearing()
        }

        setupSearchBar()
    }

    override fun onResume() {
        super.onResume()
        locationManager?.onResume()
        if (locationListener != null) {
            locationManager?.register(locationListener!!)
        }
    }

    override fun onPause() {
        LocationManager.getInstance(this).onPause()
        if (locationListener != null) {
            locationManager?.unregister(locationListener!!)
        }
        super.onPause()
    }

    private fun setupSearchBar() {
        floating_search_view.setOnQueryChangeListener({ oldQuery, newQuery ->
            if (oldQuery != "" && newQuery == "") {
                floating_search_view.clearSuggestions()
                floating_search_view.hideProgress()
            } else {
                floating_search_view.showProgress()
                DataHelper(this).findSuggestions(newQuery, 5, 250, object : DataHelper.OnFindSuggestionsListener {
                    override fun onResults(results: MutableMap<String, MapSuggestion>) {
                        floating_search_view.swapSuggestions(results.values.toMutableList())
                        floating_search_view.hideProgress()
                    }
                })
            }
        })

        floating_search_view.setOnSearchListener(object : FloatingSearchView.OnSearchListener {
            override fun onSuggestionClicked(searchSuggestion: SearchSuggestion) {
                DataHelper(this@MainActivity).findSuggestions(searchSuggestion.body, 1, 250, object : DataHelper.OnFindSuggestionsListener {
                    override fun onResults(results: MutableMap<String, MapSuggestion>) {
                        showBottomSheetBehavior(results.keys.first())
                    }
                })
                mLastQuery = searchSuggestion.body
            }

            override fun onSearchAction(query: String) {
                mLastQuery = query
                DataHelper(this@MainActivity).findSuggestions(query, 5, 250, object : DataHelper.OnFindSuggestionsListener {
                    override fun onResults(results: MutableMap<String, MapSuggestion>) {
                        floating_search_view.swapSuggestions(results.values.toMutableList())
                    }
                })
            }
        })

        floating_search_view.setOnFocusChangeListener(object : FloatingSearchView.OnFocusChangeListener {
            override fun onFocus() {
                floating_search_view.swapSuggestions(DataHelper(this@MainActivity).getHistory(3))
            }

            override fun onFocusCleared() {
                floating_search_view.setSearchBarTitle(mLastQuery)
                floating_search_view.hideProgress()
            }
        })
    }

    fun clearSearch() {
        floating_search_view.clearSuggestions()
    }

    fun clearBottomSheetBehavior() {
        mBottomSheetBehavior.state = BottomSheetBehavior.STATE_HIDDEN
    }

    fun showBottomSheetBehavior(title: String) {
        val resourceId = this.resources.getIdentifier(title, "string", this.packageName)
        title_place.text = getString(resourceId)
        val resourceIdAbout = this.resources.getIdentifier("about_" + title, "string", this.packageName)
        about_place.text = getString(resourceIdAbout)
        val resourceIdDrawable = this.resources.getIdentifier(title, "drawable", this.packageName)
        image_place.setImageResource(resourceIdDrawable)

        mBottomSheetBehavior.peekHeight = title_place.height
        mBottomSheetBehavior.state = BottomSheetBehavior.STATE_COLLAPSED
    }
}
